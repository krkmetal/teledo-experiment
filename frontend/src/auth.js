import axios from 'axios';
import JWTDecode from 'jwt-decode';

axios.defaults.baseURL = 'http://127.0.0.1:8000';
axios.defaults.timeout = 7000;
axios.defaults.headers.get['Accepts'] = 'application/json';

axios.interceptors.request.use(
  reqConfig => {
    reqConfig.headers.Authorization = localStorage.getItem('ta');

    return reqConfig;
  },
  err => Promise.reject(err),
);

const api = {

};

export default api;