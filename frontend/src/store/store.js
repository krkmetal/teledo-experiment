import Vue from "vue";
import Vuex from "vuex";
import jwt_decode from 'jwt-decode';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    userId: localStorage.getItem('uid'),
    todos: [],
    newTodo: '',
    jwtAccess: localStorage.getItem('ta'),
    jwtRefresh: localStorage.getItem('tr'),
    endpoints: {
      obtainJWT: '/auth/token/obtain/',
      refreshJWT: '/auth/token/refresh/',
    }
  },
  getters: {
    todos(state) {
      return state.todos;
    },
    newTodo(state) {
      return state.newTodo;
    },
    user(state) {
      return state.jwtAccess;
    },
    userId(state) {
      return state.userId;
    }
  },
  mutations: {
    setTodos(state, todos) {
      state.todos = todos;
    },
    setNewTodo(state, newTodo) {
      state.newTodo = newTodo;
    },
    addTodo(state, todo) {
      state.todos.push(todo);
    },
    clearNewTodo(state) {
      state.newTodo = ''
    },
    removeTodo(state, todo) {
      var todos = state.todos;
      todos.splice(todos.indexOf(todo), 1)
    },
    setUserId(state, userId) {
      localStorage.setItem('uid', userId);
      state.userId = userId;
    },
    resetUserId(state) {
      localStorage.removeItem('uid');
      state.userId = null;
    },
    setToken(state, tokens) {
      localStorage.setItem('ta', tokens.access);
      localStorage.setItem('tr', tokens.refresh);
      state.jwtAccess = tokens.access;
      state.jwtRefresh = tokens.refresh;
    },
    updateToken(state, newToken) {
      localStorage.setItem('ta', newToken);
      state.jwtAccess = newToken;
    },
    removeToken(state) {
      localStorage.removeItem('ta');
      localStorage.removeItem('tr');
      state.jwtAccess = null;
      state.jwtRefresh = null;
    }
  },
  actions: {
    loadTodos({commit, state}) {
      Vue.axios.get('/api/todo/')
        .then(response => response.data)
        .then(todos => commit('setTodos', todos))
    },
    setNewTodo({commit}, todo) {
      commit('setNewTodo', todo);
    },
    addTodo({commit, state}) {
      if (!state.newTodo) {
        return
      }
      const todo = {
        title: state.newTodo,
        description: '',
        user: state.userId,
      }
      Vue.axios.post('/api/todo/', todo).then(_ => {
        commit('addTodo', todo)
      })
    },
    clearNewTodo({commit}) {
      commit('clearNewTodo')
    },
    obtainToken({commit}, payload) {
      Vue.axios.post(this.state.endpoints.obtainJWT, payload)
        .then((response) => {
          this.commit('setToken', {
            access: response.data.access,
            refresh: response.data.refresh
          });
          const decoded = jwt_decode(response.data.access);
          const userId = decoded.user_id;
          this.commit('setUserId', userId);
          console.log(userId);
        })
        .catch((error) => {
          console.log(error);
        })
    },
    refreshToken({commit}, payload) {
      Vue.axios.post(this.state.endpoints.refreshJWT, payload)
        .then((response) => {
          this.commit('updateToken', response.data.access)
        })
        .catch((error) => {
          console.log(error)
        })
    },
    inspectToken() {
      const token = this.state.jwtAccess;
      if(token){
        const decoded = jwt_decode(token);
        const exp = decoded.exp;
        const orig_iat = decoded.orig_iat;

        if(exp - (Date.now()/1000) < 1800 && (Date.now()/1000) - orig_iat < 628200){
          this.dispatch('refreshToken')
        } else if (exp -(Date.now()/1000) < 1800){
            //this.commit('removeToken');
        } else {
           this.commit('removeToken');
           this.commit('resetUserId');
        }
      }
    }
  },
});
