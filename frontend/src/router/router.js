import Vue from "vue";
import Router from "vue-router";
import Home from "../views/pages/Home.vue";
import NotFound from "../views/pages/NotFound";

Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/profile",
      name: "profile",
      component: () =>
        import(/* webpackChunkName: "about" */ "../views/account/Profile.vue")
    },
    {
      path: "/todo",
      name: "todo",
      component: () =>
        import(/* webpackChunkName: "todo" */ "../views/pages/Todo.vue")
    },
    {
      path: "/login",
      name: "login",
      component: () =>
        import(/* webpackChunkName: "todo" */ "../views/account/Signin.vue")
    },
    { path: '*', component: NotFound },
  ]
});
