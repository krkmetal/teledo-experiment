import "@babel/polyfill";
import Vue from "vue";
import "./plugins/vuetify";
import App from "./App.vue";
import axios from "axios";
import api from "./auth";
import VueAxios from "vue-axios";
import router from "./router/router";
import store  from "./store/store";


Vue.config.productionTip = false;
Vue.use(VueAxios, axios);


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
