import telepot
from django.conf import settings
from telepot.delegate import pave_event_space, per_chat_id, create_open
from telepot.namedtuple import KeyboardButton, ReplyKeyboardMarkup
from telepot.loop import OrderedWebhook


class MessageCounter(telepot.helper.ChatHandler):
    def __init__(self, *args, **kwargs):
        super(MessageCounter, self).__init__(*args, **kwargs)

    def on_chat_message(self, msg):
        content_type, chat_type, chat_id = telepot.glance(msg)
        if content_type == 'text':
            text = msg['text']
            if text.startswith('/phone'):
                keyboard = KeyboardButton(text="Send me contact", request_contact=True)
                self.sender.sendMessage(u"Кидайте номер",
                                        reply_markup=ReplyKeyboardMarkup(keyboard=[[keyboard]], one_time_keyboard=True))
            else:
                self.sender.sendMessage("Cool story, bro!")
        elif content_type == 'contact':
            phone = msg['contact']['phone_number']
            message = "Ваш номер получен: {}".format(phone)
            self.sender.sendMessage(message)
        else:
            self.sender.sendMessage(u'Даже и не знаю, что с этим делать...')


mybot = telepot.DelegatorBot(settings.TELEGRAM_TOKEN, [
    pave_event_space()(
        per_chat_id(), create_open, MessageCounter, timeout=10,
    ),
])
mywebhook = OrderedWebhook(mybot)
if settings.TELEHOOK:
    try:
        mybot.setWebhook(settings.TELEHOOK_URL, max_connections=1)
    except telepot.exception.TooManyRequestsError:
        pass
    mywebhook.run_as_thread()
