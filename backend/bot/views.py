from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from bot.bot import mywebhook


@api_view(['POST'])
@permission_classes((AllowAny,))
def telebot(request):
    mywebhook.feed(request.data)
    return Response({'ok': True})
