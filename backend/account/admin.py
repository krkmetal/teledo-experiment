from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _

from account.forms import CustomUserChangeForm, CustomUserCreationForm
from account.models import PhoneUser


class PhoneUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('phone', 'password', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('phone', 'password1', 'password2'),
        }),
    )

    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    list_display = ('phone', 'is_staff')
    search_fields = ('phone', )
    ordering = ('phone', )


admin.site.register(PhoneUser, PhoneUserAdmin)
