from django.contrib.auth.forms import UserChangeForm, UserCreationForm

from account.models import PhoneUser


class CustomUserCreationForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        model = PhoneUser
        fields = ('phone', )


class CustomUserChangeForm(UserChangeForm):

    class Meta:
        model = PhoneUser
        fields = ('phone', )
