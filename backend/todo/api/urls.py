# coding: utf-8

from django.conf.urls import include, url
from rest_framework import routers

from todo.api.views import TodoViewSet

router = routers.DefaultRouter()
router.register(r'todo', TodoViewSet)


urlpatterns = [
    url(r'', include(router.urls, namespace='todo')),
]
