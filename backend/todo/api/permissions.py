from rest_framework import permissions


class IsOwnerPermission(permissions.BasePermission):
    """
    Object-level permission to only allow owners of an object.
    Assumes the model instance has an `user` attribute.
    """

    def has_object_permission(self, request, view, obj):
        # Instance must have an attribute named `owner`.
        print(f'{obj} and {request.user}')
        return obj.user == request.user

    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated