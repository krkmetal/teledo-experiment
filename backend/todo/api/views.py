from rest_framework import viewsets
from rest_framework_simplejwt.authentication import JWTAuthentication

from todo.api.permissions import IsOwnerPermission
from todo.api.serializers import TodoItemSerializer
from todo.models import TodoItem


class TodoViewSet(viewsets.ModelViewSet):
    queryset = TodoItem.objects.all()
    serializer_class = TodoItemSerializer
    authentication_classes = (JWTAuthentication, )
    permission_classes = (IsOwnerPermission, )

    def get_queryset(self):
        user = self.request.user
        return TodoItem.objects.filter(user=user)
