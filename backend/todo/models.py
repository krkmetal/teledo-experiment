from django.contrib.auth import get_user_model
from django.db import models


class TodoItem(models.Model):
    title = models.CharField(max_length=250)
    description = models.TextField(blank=True)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, null=True)

    class Meta:
        ordering = ['-created']

    def __str__(self):
        return self.title
