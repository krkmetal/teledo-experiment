from django.contrib import admin

from todo.models import TodoItem


@admin.register(TodoItem)
class TodoItemAdmin(admin.ModelAdmin):
    raw_id_fields = ('user', )
